import { fireEvent, render, screen } from "@testing-library/react";
import App from "./App";

test("append task list", async () => {
  render(<App />);
  const input = screen.getByPlaceholderText(/write a task/i);
  const button = screen.getByRole("button", { exact: false });
  let value, taskElement;

  value = "Get oranges";
  fireEvent.input(input, { target: { value } });
  expect(input).toHaveValue(value);
  expect(button).toBeEnabled();
  button.click();
  expect(input).toHaveValue("");
  expect(button).toBeDisabled();

  taskElement = screen.getByText(value);
  expect(taskElement).toBeInTheDocument();

  value = "Sell oranges";
  fireEvent.input(input, { target: { value } });
  button.click();

  // recheck if first element exists
  expect(taskElement).toBeInTheDocument();

  taskElement = screen.getByText(value);
  expect(taskElement).toBeInTheDocument();
});
