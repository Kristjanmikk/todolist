import { useState } from "react";
import "./App.css";

function App() {
  const [newTask, setNewTask] = useState("");
  const [taskList, setTaskList] = useState([]);
  return (
    <div className="App">
      <input
        type="text"
        placeholder="Write a task..."
        value={newTask}
        onInput={(e) => setNewTask(e.target.value)}
      />
      <button
        disabled={!newTask}
        onClick={() => {
          setTaskList([...taskList, newTask]);
          setNewTask("");
        }}
      >
        Add
      </button>
      <h1>Tasks</h1>

      <div className="List">
        {taskList.map((task, i) => (
          <div className="listItem" id={i}>
            {task}
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;
